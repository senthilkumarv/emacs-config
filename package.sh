#!/bin/bash

mkdir emacs-config
tar cvzf emacs-config/emacs-config.tar.gz --exclude='emacs-config' --exclude='*git*' --exclude='*el' .
