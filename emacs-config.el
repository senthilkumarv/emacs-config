(add-to-list 'load-path "~/.emacs.d")
(add-to-list 'load-path "~/.emacs.d/dash")
(require 'dash)
;; Added for hooking linum-mode to all major modes
(global-linum-mode)
;; Added to introduce space after line number in linum-mode
(setq linum-format "%d ")

;; Turn on parenthesis matching
(show-paren-mode 1)

;; Open Rakefile in ruby-mode
(setq auto-mode-alist (cons '("Rakefile" . ruby-mode) auto-mode-alist))

;; Open Capfile in ruby-mode
(setq auto-mode-alist (cons '("Capfile" . ruby-mode) auto-mode-alist))

;; Open .rake tasks in ruby-mode
(setq auto-mode-alist (cons '("\\.rake$" . ruby-mode) auto-mode-alist))

;; Open .rhtml tasks in ruby-mode
(setq auto-mode-alist (cons '("\\.rhtml$" . ruby-mode) auto-mode-alist))

;; Open .erb tasks in ruby-mode
(setq auto-mode-alist (cons '("\\.erb$" . ruby-mode) auto-mode-alist))

;; Open .emacs in lisp-mode
(setq auto-mode-alist (cons '(".emacs$" . lisp-mode) auto-mode-alist))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-save-file-name-transforms (quote ((".*" "~/.emacs.d/autosaves/\\1" t))))
 '(backup-directory-alist (quote ((".*" . "~/.emacs.d/backups/"))))
 '(ecb-options-version "2.40")
 '(inhibit-startup-screen t))

;; Put autosave files (ie #foo#) and backup files (ie foo~) in ~/.emacs.d/.


;; create the autosave dir if necessary, since emacs won't.
(make-directory "~/.emacs.d/autosaves/" t)

;; Add yasnippets support

(add-to-list 'load-path "~/.emacs.d/yasnippet")

(require 'yasnippet)
(setq yas-snippet-dirs '("~/.emacs.d/yasnippet/snippets"))
;; Load the snippets
(setq yas/root-directory "~/.emacs.d/yasnippet/snippets")
(yas/load-directory yas/root-directory)
(yas/initialize)

(defun yas/popup-isearch-prompt (prompt choices &optional display-fn)
  (when (featurep 'popup)
    (popup-menu*
     (mapcar
      (lambda (choice)
        (popup-make-item
         (or (and display-fn (funcall display-fn choice))
             choice)
         :value choice))
      choices)
     :prompt prompt
     ;; start isearch mode immediately
     :isearch t
     )))
 
(setq yas/prompt-functions '(yas/popup-isearch-prompt yas/no-prompt))
(require 'auto-complete-config)
(add-to-list 'load-path "~/.emacs.d")    ; This may not be appeared if you have already added.
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(ac-config-default)
(global-auto-complete-mode t)

;; Enable flyspell while in text-mode
(add-hook 'text-mode-hook 'flyspell-mode)
(setq-default ispell-program-name "aspell")

(require 'autopair)
(autopair-global-mode) ;; enable autopair in all buffers

;;key bindings
(global-set-key [f2] 'split-window-horizontally )
(global-set-key [f3] 'split-window-vertically )
(global-set-key [f4] 'query-replace)
(global-set-key "\C-l" 'other-window)
(global-set-key (kbd "M-p") 'scroll-down)
(global-set-key (kbd "M-n") 'scroll-up)

;;Move windows
(windmove-default-keybindings 'meta)
(define-key input-decode-map "\e\e[A" [(meta up)])
(define-key input-decode-map "\e\e[B" [(meta down)])
(define-key input-decode-map "\e\e[D" [(meta left)])
(define-key input-decode-map "\e\e[C" [(meta right)])
(define-key input-decode-map "\e\eOA" [(meta up)])
(define-key input-decode-map "\e\eOB" [(meta down)])
(define-key input-decode-map "\e\eOD" [(meta left)])
(define-key input-decode-map "\e\eOC" [(meta right)])

;;Terminal.app charset
(set-keyboard-coding-system nil)
(set-terminal-coding-system 'utf-8)


;;Bind M-x to C-x C-m
(global-set-key "\C-x\C-m" 'execute-extended-command)

;;Add lusty explorer support
(add-to-list 'load-path "~/.emacs.d/lusty-explorer")
(require 'lusty-explorer)

;; overrride the normal file-opening, buffer switching
(global-set-key (kbd "C-x C-f") 'lusty-file-explorer)
(global-set-key (kbd "C-x b")   'lusty-buffer-explorer)

;;Pasteboard integration
(defun mac-copy ()
  (shell-command-to-string "pbpaste"))

(defun mac-paste (text &optional push)
  (let ((process-connection-type nil))
    (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
      (process-send-string proc text)
      (process-send-eof proc))))

(setq interprogram-cut-function 'mac-paste)
(setq interprogram-paste-function 'mac-copy)

;;Move line

(defun move-line (n)
  "Move the current line up or down by N lines."
  (interactive "p")
  (setq col (current-column))
  (beginning-of-line) (setq start (point))
  (end-of-line) (forward-char) (setq end (point))
  (let ((line-text (delete-and-extract-region start end)))
    (forward-line n)
    (insert line-text)
    ;; restore point to original column in moved line
    (forward-line -1)
    (forward-char col)))

(defun move-line-up (n)
  "Move the current line up by N lines."
  (interactive "p")
  (move-line (if (null n) -1 (- n))))

(defun move-line-down (n)
  "Move the current line down by N lines."
  (interactive "p")
  (move-line (if (null n) 1 n)))

(global-set-key (kbd "C-c <up>") 'move-line-up)
(global-set-key (kbd "C-c <down>") 'move-line-down)

;;Emacs nav-49 mode

(add-to-list 'load-path "~/.emacs.d/emacs-nav-49")
(require 'nav)
(nav-disable-overeager-window-splitting)
(global-set-key [f8] 'nav-toggle)

;; Make C-g as Goto

(global-set-key (kbd "C-g") 'goto-line)

;; Kill other buffers
(setq stack-trace-on-error t)
(add-to-list 'load-path "~/.emacs.d/ecb")
(require 'ecb)

;; Mark multiple
(add-to-list 'load-path "~/.emacs.d/mark-multiple")
(require 'inline-string-rectangle)

;; Multiple cursors
(add-to-list 'load-path "~/.emacs.d/multiple-cursors")
(require 'multiple-cursors)

;; Expand region
(add-to-list 'load-path "~/.emacs.d/expand-region")
(require 'expand-region)
(global-set-key (kbd "C-M-e") 'er/expand-region)
(global-set-key (kbd "C-M-c") 'er/contract-region)
;; js2-mode

(add-to-list 'load-path "~/.emacs.d/js2-mode")
(autoload 'js2-mode "js2-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))

;; js2-refactor
(add-to-list 'load-path "~/.emacs.d/s.el")
(add-to-list 'load-path "~/.emacs.d/mark-multiple")
(add-to-list 'load-path "~/.emacs.d/expand-region")
(add-to-list 'load-path "~/.emacs.d/js2-refactor")
(require 'js2-refactor)
(js2r-add-keybindings-with-prefix "C-c C-m")

;; js2-highlight-vars
(require 'js2-highlight-vars)
(if (featurep 'js2-highlight-vars)
  (js2-highlight-vars-mode))

;; JSLint mode
(add-to-list 'load-path "~/.emacs.d/jshint-mode")
(require 'flymake-jshint)
(add-hook 'js2-mode-hook
     (lambda () (flymake-mode t)))
(add-hook 'find-file-hook 'flymake-find-file-hook)

;; Duplicate lines using C-c C-d
(defun duplicate-current-line-or-region (arg)
  "Duplicates the current line or region ARG times.
If there's no region, the current line will be duplicated. However, if
there's a region, all lines that region covers will be duplicated."
  (interactive "p")
  (let (beg end (origin (point)))
    (if (and mark-active (> (point) (mark)))
        (exchange-point-and-mark))
    (setq beg (line-beginning-position))
    (if mark-active
        (exchange-point-and-mark))
    (setq end (line-end-position))
    (let ((region (buffer-substring-no-properties beg end)))
      (dotimes (i arg)
        (goto-char end)
        (newline)
        (insert region)
        (setq end (point)))
      (goto-char (+ origin (* (length region) arg) arg)))))

(global-set-key (kbd "C-c C-d") 'duplicate-current-line-or-region)

;; Setup tabs
(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
(custom-set-variables  
 '(js2-basic-offset 2)  
 '(js2-bounce-indent-p t)  
)
;; FanCy looking stuff

(display-battery-mode 1)
(display-time-mode 1)

;; Scala mode

(add-to-list 'load-path "~/.emacs.d/scala-mode-2")
(require 'scala-mode2)
(add-hook 'scala-mode2-hook
            '(lambda ()
               (yas/minor-mode-on)
               ))

;; Multiple cursors keybindings

(global-set-key (kbd "C-x C-e") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-l") 'mc/mark-all-like-this)
(global-set-key (kbd "C-x C-M-e") 'multiple-cursors-mode)

;; Tidy

(autoload 'tidy-buffer "tidy" "Run Tidy HTML parser on current buffer" t)
(autoload 'tidy-parse-config-file "tidy" "Parse the `tidy-config-file'" t)
(autoload 'tidy-save-settings "tidy" "Save settings to `tidy-config-file'" t)
(autoload 'tidy-build-menu  "tidy" "Install an options menu for HTML Tidy." t)

(defun my-html-mode-hook () "Customize my html-mode."
  (tidy-build-menu html-mode-map)
  (local-set-key [(control c) (control c)] 'tidy-buffer)
  (setq sgml-validate-command "tidy"))

(add-hook 'html-mode-hook 'my-html-mode-hook)

;; Projectile
(require 'package)
(add-to-list 'package-archives
  '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives 
    '("marmalade" .
      "http://marmalade-repo.org/packages/"))
(package-initialize)

(require 'projectile)
(projectile-global-mode)
(setq projectile-enable-caching t)

;;Magit
(add-to-list 'load-path "~/.emacs.d/magit")
(require 'magit)
(global-set-key (kbd "C-c g s") 'magit-status)

;; change magit diff colors
(eval-after-load 'magit
  '(progn
     (set-face-foreground 'magit-diff-add "green3")
     (set-face-foreground 'magit-diff-del "red3")
     (when (not window-system)
       (set-face-background 'magit-item-highlight "black"))))

;; Adding pending delete mode
(delete-selection-mode 1)

;; Turn-off autopair mode on js2-mode
(eval-after-load "autopair"
  '(progn
     (autopair-global-mode 1)

     (setq my-autopair-off-modes '(js2-mode))
     (dolist (m my-autopair-off-modes)
       (add-hook (intern (concat (symbol-name m) "-hook"))
                 #'(lambda () (setq autopair-dont-activate t))))
     ))

;; Set command key as super key
(setq mac-command-modifier 'super) 
