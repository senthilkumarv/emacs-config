(add-to-list 'load-path ".")
(add-to-list 'load-path "./autopair")
(add-to-list 'load-path "./ecb")
(add-to-list 'load-path "./emacs-nav-49")
(add-to-list 'load-path "./dash")
(add-to-list 'load-path "./mark-multiple")
(add-to-list 'load-path "./expand-region")
(add-to-list 'load-path "./js2-mode")
(add-to-list 'load-path "./js2-refactor")
(add-to-list 'load-path "./lintnode")
(add-to-list 'load-path "./yasnippet")
(add-to-list 'load-path "./rsense/etc")
(add-to-list 'load-path "./ansi")
(add-to-list 'load-path "./js2-refactor/util/espuds")
(add-to-list 'load-path "./js2-refactor/util/ecukes")
(add-to-list 'load-path "./mark-multiple")
(add-to-list 'load-path "./multiple-cursors")

(setq ecukes-path "./js2-refactor/util/ecukes")
(setq js2-refactor-root-path "./js2-refactor")
(setq js2-refactor-util-path "./js2-refactor/util")

(require 'dash)
(require 'ansi)
(require 'mark-multiple)
(require 'expand-region)
(require 'js2-refactor)

(setq byte-compile-warnings '(not nresolved
                                  free-vars
                                  callargs
                                  redefine
                                  obsolete
                                  noruntime
                                  cl-functions
                                  interactive-only
                                  ))
(byte-recompile-directory "." 0)
